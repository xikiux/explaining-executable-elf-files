# Explaining Executable Elf Files

An explanation of how to create an executable elf file using C++ without the use of an assembler.

Since I have not found any online tutorials that talk about how to make an executable elf file without the use of an assembler I thought I'd make one.

*Note: this tutorial will not cover everything related to elf files.
It is simply a tutorial to get you started on making your own executable elf files from scratch, this is not comprehensive.

# Things you should be familiar with

* C++
* The basics of binary
* x86_64 / AMD64 Assembly
* The linux command line
* Make files

# Structure of an Elf File

An executable elf file is made up of mainly 5 things:
* One elf header
* One or more program headers side by side in a table called the program header table
* One or more section headers side by side in a table called the section header table
* One or more sections
* One or more segments

The elf header is the master header that references the program header table and section header table.
A program header is a pointer to one segment in the elf file.
A section header is a pointer to one section in the elf file.
A section is a buffer of data that can be used for anything. (Executable code, string storage, etc...)
A segment is a pointer to one or more sections grouped together to be loaded into memory before runtime.

The structure of the elf file can be rearanged, so long as the elf header is at the very beginning of the file.
This is how our elf file will be structured.
Each pipe '|' means that it is next to the other item on file in bytes at an alignment of 4 bytes.
Alignment means that the first byte of each item is a multiple of some number (in this case 4).
Each square bracket means that there is a table made up of the thing inside of the square brackets.
Each item in the table is side by side with each other with zero bytes in between.
Each '...' means that there is no table for each item but they happen to be next to each other at an alignment of 4 bytes,
there is an arbitrary amount of them.
EOF means that it is the end of the file and there are no more bytes.

Elf header | [ Program header ] | [ Section header ] | Section ... | EOF

First we have the elf header at byte offset 0.
Then we have the program header table directly after the elf header, which contains all of our program headers.
Then we have the section header table directly after the program header table, which contains all of our section headers.
Then we have a list of sections directly after the section header table
Then the file ends with some padding to the last section to keep the file alignment.

# How our elf file will be structured more specifically

Our elf file will have 7 things:
* One elf header
* One program header to load the executable section into memory as a segment
* One blank section header because the elf standard requires it
* One section header referencing the section containing one string table for the names of each section on file 
* One section header referencing the section containing our executable code
* One section for our string table which contains the names of our elf file sections. (.shstrtab)
* One section for our executable code (.text)

# Creating an elf file

We will need 5 unique classes for our program.
* A class representing the entire elf file ("example/code/elf_file.cpp")
* A class for the elf header ("example/code/elf_header.cpp")
* A class for one program header ("example/code/elf_program_header.cpp")
* A class for one section header ("example/code/elf_section_header.cpp")
* A class for a section ("example/code/elf_section.cpp")

And we need one file to use the entire elf file class
* Main file ("example/code/main.cpp")

All of the code will be inside the example folder.

Then we need to build the project,
which can be done by typing in "make debug" in the command line inside the example folder.
Then we need to assemble the executable by typing in "./assemble" into the command line.
And if all is well then type in "./elf_out" and "echo $?". If the only thing that shows in the command line is the number 2 then you did it! Hooray!

*Note: This is not the end of the tutorial, it's incomplete.