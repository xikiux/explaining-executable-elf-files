// Written by Bradford Shapleigh (https://gitlab.com/xikiux)

#include <vector>

#include <elf.h>

#include "elf_header.cpp"

namespace elf_assembler {
	class elf_file {
		elf_header* m_elf_header;
		std::vector<Elf64_Phdr*>* m_program_header_table;
		std::vector<Elf64_Shdr*>* m_section_header_table;
		std::vector<std::vector<uint8_t>*>* m_sections;
		std::vector<const char*>* m_section_names;

	public:

		elf_file() {
			m_section_names = new std::vector<const char*>();

			m_elf_header = new elf_header();
			m_program_header_table = new std::vector<Elf64_Phdr*>();
			m_section_header_table = new std::vector<Elf64_Shdr*>();
			m_sections = new std::vector<std::vector<uint8_t>*>();
		}

		void add_section(std::vector<uint8_t>* section_data, const char* section_name) {
			m_sections->push_back(section_data);
			m_section_names->push_back(section_name);
		}

		void add_segment() {
			// TODO;
		}

		std::vector<uint8_t>* serialize() {
			std::vector<uint8_t>* output = new std::vector<uint8_t>();
			std::vector<uint8_t>* temp;

			m_elf_header->initialize(0, 0, 0, 0, 0, 0);

			temp = m_elf_header->serialize();
			for (uint8_t byte : *temp)
				output->push_back(byte);
			delete temp;

			return output;
		}
	};
}