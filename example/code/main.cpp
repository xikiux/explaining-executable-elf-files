// Written by Bradford Shapleigh (https://gitlab.com/xikiux)

#include <iostream>
#include <fstream>
#include <vector>

#include "elf_file.cpp"

int overwrite_to_binary_file(const char* file_path, std::vector<uint8_t>* buffer) {
	std::ofstream file;

	file.open(file_path, std::ios::binary | std::ios::out | std::ios::trunc);

	if (file.is_open()) {
		for (uint8_t byte : *buffer) {
			file << byte;
		}

		file.close();
		return 0;
	} else {
		std::cout << "Enable to open file for writing!" << std::endl;
		return 1;
	}
}

int main() {
	std::vector<uint8_t>* text_section = new std::vector<uint8_t>({ 0x48, 0xC7, 0xC0, 0x3C, 0x00, 0x00, 0x00, 0x48, 0xC7, 0xC7, 0x02, 0x00, 0x00, 0x00, 0x0F, 0x05 });
	std::vector<uint8_t>* string_table = new std::vector<uint8_t>();
	elf_assembler::elf_file* elf = new elf_assembler::elf_file();

	elf->add_section(text_section, ".text");
	elf->add_section(string_table, ".shstrtab");

	return overwrite_to_binary_file("elf_out", elf->serialize());
}