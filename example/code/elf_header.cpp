// Written by Bradford Shapleigh (https://gitlab.com/xikiux)

#include <vector>
#include <cstring>
#include <type_traits>

#include <elf.h>

namespace elf_assembler {
	class elf_header {
		Elf64_Ehdr* m_header;
	public:

		elf_header() {
			m_header = new Elf64_Ehdr();
		}

		// fill out the elf header struct
		void initialize(
			Elf64_Addr entry_point,
			Elf64_Off program_header_table_file_offset,
			Elf64_Off section_header_table_file_offset,
			Elf64_Half program_table_entry_count,
			Elf64_Half section_table_entry_count,
			Elf64_Half string_table_section_header_table_index)
		{
			// initialize e_ident
			// magic number
			m_header->e_ident[0] = ELFMAG0;
			m_header->e_ident[1] = ELFMAG1;
			m_header->e_ident[2] = ELFMAG2;
			m_header->e_ident[3] = ELFMAG3;
			// class (whether our file is 32bit or 64 bit)
			m_header->e_ident[4] = ELFCLASS64;
			// endian-ness (what order the bits are in)
			m_header->e_ident[5] = ELFDATA2LSB;
			// is current version of elf
			m_header->e_ident[6] = EV_CURRENT;
			// what operating system basically (abi - application binary interface)
			m_header->e_ident[7] = ELFOSABI_LINUX;
			// abi version
			m_header->e_ident[8] = 0;
			// padding (unused bytes)
			m_header->e_ident[9] = 0;
			m_header->e_ident[10] = 0;
			m_header->e_ident[11] = 0;
			m_header->e_ident[12] = 0;
			m_header->e_ident[13] = 0;
			m_header->e_ident[14] = 0;
			// the size of the ident array
			m_header->e_ident[15] = sizeof(m_header->e_ident);

			// initialize the rest of the elf header
			// set the elf file type to executable
			m_header->e_type = ET_EXEC;
			// set the machine architecture to your current architecture, this tutorial will be using x86_64
			m_header->e_machine = EM_X86_64;
			// we are using the current verison of our software
			m_header->e_version = EV_CURRENT;
			// the entry point for our executable
			m_header->e_entry = entry_point;
			m_header->e_phoff = program_header_table_file_offset;
			m_header->e_shoff = section_header_table_file_offset;
			// the processor specific flags, none exits / are needed for x86_64
			m_header->e_flags = 0;
			// size of the elf header
			m_header->e_ehsize = sizeof(Elf64_Ehdr);
			// size of one program header
			m_header->e_phentsize = sizeof(Elf64_Phdr);
			// amount of program headers
			m_header->e_phnum = program_table_entry_count;
			// size of one section header
			m_header->e_shentsize = sizeof(Elf64_Shdr);
			// amount of section headers
			m_header->e_shnum = section_table_entry_count;
			// section header table index of string table for section names (.shstrtab)
			m_header->e_shstrndx = string_table_section_header_table_index;
		}

		// turn the header into binary
		std::vector<uint8_t>* serialize() {
			std::vector<uint8_t>* binary = new std::vector<uint8_t>();
			uint8_t temp;

			for (unsigned long long i = 0; i < sizeof(Elf64_Ehdr); i++) {
				memcpy(&temp, (Elf64_Ehdr*)((unsigned long long)m_header + i), sizeof(uint8_t));
				binary->push_back(temp);
			}

			return binary;
		}
	};
}